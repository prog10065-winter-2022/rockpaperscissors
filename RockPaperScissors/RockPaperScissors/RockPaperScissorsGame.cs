﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RockPaperScissors
{
    //this is an enum which takes the computer and player choice and assigns it a value of Rock, Paper, or Scissors
    public enum RockPaperScissorsOptions
    {
        Rock = 1,
        Paper = 2,
        Scissors = 3,
    }


    internal class RockPaperScissorsGame
    {
      
       // in order to make the code cleaner the three options are assigned to field variables which makes it less overall code written "RockPaperScissorsOptions.Scissors is logner than _scissors
        public RockPaperScissorsOptions _rock = RockPaperScissorsOptions.Rock;
        public RockPaperScissorsOptions _paper = RockPaperScissorsOptions.Paper;
        public RockPaperScissorsOptions _scissors = RockPaperScissorsOptions.Scissors;


        int _maxScore;

        public int _playerScore;
        public int _computerScore;

        public bool _isGameOver;

        public bool _playerWin;
        public bool _computerWin;


        public RockPaperScissorsGame(int maxScore)
        {
            MaxScore = maxScore;

        }

        public int MaxScore
        {
            get => _maxScore;
            private set
            {
                if (value < 0)
                    throw new Exception("The game must include at least one round");
                _maxScore = value;
            }
        }

        //this function returns the score of the side which won the round + 1 depending on the choices selected by both
        //if its a tie it does not add anything to either score
        //otherwise it throws an exception
        public int OutcomeOfRound(RockPaperScissorsOptions playersChoice, RockPaperScissorsOptions computersChoice)
        {

            if (playersChoice == _rock && computersChoice == _scissors)
            {
                return _playerScore++;

            }
            else if (playersChoice == _rock && computersChoice == _paper)
            {

                return _computerScore++;
            }

            else if (playersChoice == _paper && computersChoice == _rock)
            {
                return _playerScore++;
            }

            else if (playersChoice == _paper && computersChoice == _scissors)
            {
                return _computerScore++;
            }

            else if (playersChoice == _scissors && computersChoice == _paper)
            {

                return _playerScore++;

            }
            else if (playersChoice == _scissors && computersChoice == _rock)
            {

                return _computerScore++;
            }

            else if (playersChoice == computersChoice)

            {
                return _computerScore;
            }
            else
            {
                throw new Exception("Invalid Button Press");
            }


        }

        //everytime the button is clicked it checks whether or not either of the 2 scores have reached the max score
        //if one of them has it returns that the game is over and that side has won both as bools
        public bool OutcomeOfGame()
        {
            if (_isGameOver)
                throw new Exception("The game is already over");

            if (_playerScore == _maxScore)
            {
                _playerWin = true;
                return _isGameOver = true;

            }
            else if (_computerScore == _maxScore)
            {
                _computerWin = true;
                return _isGameOver = true;

            }
            else
            {
                return _isGameOver = false;
            }
        }

        //these are accessor and mutator methods for the field variables to be accessed in the MainPage.xaml.cs

        public int PlayerScore => _playerScore;

        public int ComputerScore => _computerScore;

       public bool IsGameOver
        {
            get => _isGameOver;


            set => _isGameOver = value;
        }

       public bool PlayerWin
        {
            get => _playerWin;
            set => _playerWin = value;
        }

       public bool ComputerWin
        {
            get => _computerWin;
            set => _computerWin = value;
        }
        
    }




}
